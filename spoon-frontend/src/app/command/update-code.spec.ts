import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { UpdateCode } from './update-code';
import { HttpClient } from '@angular/common/http';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { ASTNode } from '../component/ast/ast.component';

const testData: ASTNode[] = [
  {
    label: 'Fruit',
    children: [
      {label: 'Apple'},
      {label: 'Banana'},
      {label: 'Fruit loops'},
    ]
  }, {
    label: 'Vegetables',
    children: [
      {
        label: 'Green',
        children: [
          {label: 'Broccoli'},
          {label: 'Brussels sprouts'},
        ]
      }, {
        label: 'Orange',
        children: [
          {label: 'Pumpkins'},
          {label: 'Carrots'},
        ]
      },
    ]
  },
];

describe('UpdateCode', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let updateCode: UpdateCode;
  const dataSource = new MatTreeNestedDataSource<ASTNode>();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HttpClient],
    });

    dataSource.data = testData;
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    updateCode = new UpdateCode(httpClient, dataSource);
  });

  it('should create', () => {
    expect(updateCode).toBeTruthy();
  });

  it('should make a HTTP request', () => {
    const testCode = 'test';
    const httpResponse: ASTNode = {
      label: 'test',
      children: []
    };

    updateCode.code = testCode;
    updateCode.doIt();

    const req = httpTestingController.expectOne({
      method: 'POST',
      url: 'spoon/ast',
    });

    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual({ code: testCode, level: 'a' });

    req.flush(httpResponse);
    httpTestingController.verify();
    expect(dataSource.data).toEqual([httpResponse]);
  });
});
