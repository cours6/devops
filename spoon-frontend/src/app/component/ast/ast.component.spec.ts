import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { AstComponent, ASTNode } from './ast.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatTreeModule } from '@angular/material/tree';
import { MatIconModule } from '@angular/material/icon';
// import { HarnessLoader } from '@angular/cdk/testing';
// import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';

const testData: ASTNode[] = [
  {
    label: 'Fruit',
    children: [
      {label: 'Apple'},
      {label: 'Banana'},
      {label: 'Fruit loops'},
    ]
  }, {
    label: 'Vegetables',
    children: [
      {
        label: 'Green',
        children: [
          {label: 'Broccoli'},
          {label: 'Brussels sprouts'},
        ]
      }, {
        label: 'Orange',
        children: [
          {label: 'Pumpkins'},
          {label: 'Carrots'},
        ]
      },
    ]
  },
];

describe('AstComponent', () => {
  let component: AstComponent;
  let fixture: ComponentFixture<AstComponent>;

  // let loader: HarnessLoader;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AstComponent ],
      imports: [
        HttpClientTestingModule,
        MatTreeModule,
        MatIconModule,
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AstComponent);
    component = fixture.componentInstance;
    // loader = TestbedHarnessEnvironment.loader(fixture);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should construct', () => {
    expect(component.dataSource).toBeDefined();
    expect(component.dataSource.data).toEqual(testData);
    expect(component.treeControl).toBeDefined();
  });

  it('should render HTML', () => {
    const astElement: HTMLElement = fixture.debugElement.nativeElement;

    expect(astElement.querySelector('mat-tree')).toBeDefined();
    expect(astElement.querySelector('textarea')).toBeDefined();
  });

  it('should bind the testData to the mat-tree HTML element', () => {
    const astElement: HTMLElement = fixture.debugElement.nativeElement;
    const matTree: HTMLElement = astElement.querySelector('mat-tree');

    expect(matTree.children.length).toEqual(2);
    expect(matTree.children[0].textContent).toContain(testData[0].label);
    expect(matTree.children[1].textContent).toContain(testData[1].label);
  });

  it('should bind updateCodeCall (once initialized)', () => {
    jasmine.clock().install();
    const astElement: HTMLElement = fixture.debugElement.nativeElement;
    const textarea: HTMLTextAreaElement = astElement.querySelector<HTMLTextAreaElement>('textarea');
    const thenCallbackSpy = spyOn(component, 'thenCallback');

    expect(thenCallbackSpy).not.toHaveBeenCalled();
    textarea.textContent = 'new value';
    textarea.dispatchEvent(new Event('input'));
    jasmine.clock().tick(2000);
    fixture.detectChanges();
    expect(thenCallbackSpy).toHaveBeenCalled();
    jasmine.clock().uninstall();
  });
});
