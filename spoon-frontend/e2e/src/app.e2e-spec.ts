import { AppPage } from './app.po';
import { browser, by, ElementFinder, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display a welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Spoon AST visualiser');
  });

  it('should have an empty areatext first', () => {
    page.navigateTo();
    expect(page.getTextArea().getText()).toEqual('');
  });

  it('should type in areatext', () => {
    page.navigateTo();
    const textarea = page.getTextArea();
    textarea.sendKeys('new text');
    expect(textarea.getAttribute('value')).toEqual('new text');
  });

  it('should have a predefined tree result first', () => {
    /*
    const labels = ['Fruit', 'Apple', 'Banana',
    'Fruit loops', 'Vegetables','Green',
    'Broccoli', 'Brussels sprouts', 'Orange',
    'Pumpkins', 'Carrots'];
    */
    page.navigateTo();
    const matTree = page.getMatTree();
    expect(matTree).toBeDefined();
  });

  /*
  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
   */
});
