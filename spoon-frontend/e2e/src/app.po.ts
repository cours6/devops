import { browser, by, element, ElementArrayFinder, ElementFinder } from 'protractor';

export class AppPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getTitleText(): Promise<string> {
    return element(by.tagName('h1')).getText() as Promise<string>;
  }

  getTextArea(): ElementFinder {
    return element(by.tagName('textarea'));
  }

  getMatTree(): ElementFinder {
    return element(by.tagName('mat-tree'));
  }
}
