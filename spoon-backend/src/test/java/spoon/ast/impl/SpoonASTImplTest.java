package spoon.ast.impl;

import org.eclipse.jdt.core.dom.ASTNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import spoon.ast.api.SpoonAST;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class SpoonASTImplTest {

    SpoonASTImpl spoonAST;

    @BeforeEach
    void setUp() {
        this.spoonAST = new SpoonASTImpl("label", "tooltip", 0, 10);
    }

    @Test
    void getLabel() {
        assertEquals("label", this.spoonAST.getLabel());
    }

    @Test
    void getStartPosition() {
        assertEquals(0, this.spoonAST.getStartPosition());
    }

    @Test
    void getEndPosition() {
        assertEquals(10, this.spoonAST.getEndPosition());
    }

    @Test
    void getTooltip() {
        assertEquals("tooltip", this.spoonAST.getTooltip());
    }

    @Test
    void getChildren() {
        assertEquals(0, this.spoonAST.getChildren().size());
    }

    @Test
    void addChild() {
        SpoonAST spoonChild = new SpoonASTImpl("child", "tooltip", 10, 20);
        this.spoonAST.addChild(spoonChild);
        assertEquals(1, this.spoonAST.getChildren().size());
        assertEquals(spoonChild, this.spoonAST.getChildren().get(0));
    }

    @Test
    void removeChild() {
        SpoonAST spoonChild = new SpoonASTImpl("child", "tooltip", 10, 20);
        this.spoonAST.addChild(spoonChild);
        assertEquals(1, this.spoonAST.getChildren().size());
        this.spoonAST.removeChild(null);
        assertEquals(1, this.spoonAST.getChildren().size());
        this.spoonAST.removeChild(spoonChild);
        assertEquals(0, this.spoonAST.getChildren().size());
    }

    @Test
    void getParent() {
    }

    @Test
    void setParent() {
    }

    @Test
    void testToString() {
    }
}