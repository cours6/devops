package spoon.ast.api;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class TreeLevelTest {

    @Test
    void AutoTreeLevelTest() {
        Optional<TreeLevel> ol = TreeLevel.from("a");
        assertTrue(ol.isPresent());
        TreeLevel l = ol.get();
        assertAll(
                () -> assertEquals(TreeLevel.AUTO, l),
                () -> assertEquals("Auto", l.toString())
        );
    }

    @Test
    void ClassElementTreeLevelTest() {
        Optional<TreeLevel> ol = TreeLevel.from("e");
        assertTrue(ol.isPresent());
        TreeLevel l = ol.get();
        assertAll(
                () -> assertEquals(TreeLevel.CLASS_ELEMENT, l),
                () -> assertEquals("Class element", l.toString())
        );
    }

    @Test
    void StatementTreeLevelTest() {
        Optional<TreeLevel> ol = TreeLevel.from("s");
        assertTrue(ol.isPresent());
        TreeLevel l = ol.get();
        assertAll(
                () -> assertEquals(TreeLevel.STATEMENT, l),
                () -> assertEquals("Statement", l.toString())
        );
    }

    @Test
    void ExpressionTreeLevelTest() {
        Optional<TreeLevel> ol = TreeLevel.from("x");
        assertTrue(ol.isPresent());
        TreeLevel l = ol.get();
        assertAll(
                () -> assertEquals(TreeLevel.EXPRESSION, l),
                () -> assertEquals("Expression", l.toString())
        );
    }

    @Test
    void EmptyTreeLevelTest() {
        Optional<TreeLevel> ol = TreeLevel.from("z");
        assertTrue(ol.isEmpty());
    }

}