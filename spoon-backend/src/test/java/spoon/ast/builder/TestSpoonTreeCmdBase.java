package spoon.ast.builder;

import org.junit.jupiter.api.Test;
import spoon.ast.api.TreeLevel;
import spoon.reflect.CtModel;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TestSpoonTreeCmdBase {
	SpoonTreeCmdBase analyser;

	@Test
	void buildEmptyClass() {
		analyser = new SpoonTreeCmdBase(true, "public class Foo {}", TreeLevel.CLASS_ELEMENT);
		final var res = analyser.execute().orElseThrow();
		assertThat(res.getChildren()).hasSize(1);
		assertThat(res.getChildren().get(0).getChildren()).hasSize(0);
		assertThat(res.getChildren().get(0).getLabel()).isEqualTo("CtClass (role: typeMember) : Foo");
	}

	@Test
	void buildClassWithTwoAttributes() {
		analyser = new SpoonTreeCmdBase(true, "public class Foo {int foo1; String foo2}", TreeLevel.CLASS_ELEMENT);
		final var res = analyser.execute().orElseThrow().getChildren().get(0).getChildren();
		assertThat(res).hasSize(2);
		assertThat(res.get(0).getLabel()).isEqualTo("CtField (role: typeMember) : foo1");
		assertThat(res.get(1).getLabel()).isEqualTo("CtField (role: typeMember) : foo2");
	}

	@Test
	void buildFunctionLevel() {
		analyser = new SpoonTreeCmdBase(true, "public void test() {}", TreeLevel.CLASS_ELEMENT);
		final var res = analyser.execute().orElseThrow();
		assertThat(res.getChildren()).hasSize(1);
		assertThat(res.getChildren().get(0).getChildren()).hasSize(2);
		assertThat(res.getChildren().get(0).getLabel()).isEqualTo("CtMethod (role: typeMember) : test");
		assertThat(res.getChildren().get(0).getChildren().get(0).getLabel()).isEqualTo("CtTypeReference (role: type) : void");
		assertThat(res.getChildren().get(0).getChildren().get(0).getChildren()).hasSize(0);
		assertThat(res.getChildren().get(0).getChildren().get(1).getLabel()).isEqualTo("CtBlock (role: body)");
		assertThat(res.getChildren().get(0).getChildren().get(1).getChildren()).hasSize(0);
	}

	@Test
	void buildFunctionLevel2() {
		analyser = new SpoonTreeCmdBase(true, "public void test() {int a = 1;}", TreeLevel.CLASS_ELEMENT);
		final var res = analyser.execute().orElseThrow();
		assertThat(res.getChildren()).hasSize(1);
		assertThat(res.getChildren().get(0).getChildren()).hasSize(2);
		assertThat(res.getChildren().get(0).getLabel()).isEqualTo("CtMethod (role: typeMember) : test");
		assertThat(res.getChildren().get(0).getChildren().get(0).getLabel()).isEqualTo("CtTypeReference (role: type) : void");
		assertThat(res.getChildren().get(0).getChildren().get(0).getChildren()).hasSize(0);
		assertThat(res.getChildren().get(0).getChildren().get(1).getLabel()).isEqualTo("CtBlock (role: body)");
		assertThat(res.getChildren().get(0).getChildren().get(1).getChildren()).hasSize(1);
	}

	/**
	 * TODO :: Need to check
	 */
	@Test
	void buildFunctionLevelWithAnnotation() {
		analyser = new SpoonTreeCmdBase(true, "@Test public void test() {}", TreeLevel.CLASS_ELEMENT);
		final var res = analyser.execute().orElseThrow();
		assertThat(res.getChildren()).hasSize(1);
		assertThat(res.getChildren().get(0).getChildren()).hasSize(3);
		assertThat(res.getChildren().get(0).getLabel()).isEqualTo("CtMethod (role: typeMember) : test");

		assertThat(res.getChildren().get(0).getChildren().get(0).getLabel()).isEqualTo("CtAnnotation (role: annotation) : Test");
		assertThat(res.getChildren().get(0).getChildren().get(0).getChildren()).hasSize(2);

		assertThat(res.getChildren().get(0).getChildren().get(1).getLabel()).isEqualTo("CtTypeReference (role: type) : void");
		assertThat(res.getChildren().get(0).getChildren().get(1).getChildren()).hasSize(0);
		assertThat(res.getChildren().get(0).getChildren().get(2).getLabel()).isEqualTo("CtBlock (role: body)");
		assertThat(res.getChildren().get(0).getChildren().get(2).getChildren()).hasSize(0);
	}

	@Test
	void buildStatementLevel() {
		analyser = new SpoonTreeCmdBase(true, "int a = 1;", TreeLevel.STATEMENT);
		final var res = analyser.execute().orElseThrow();
		assertThat(res.getChildren()).hasSize(1);
		assertThat(res.getChildren().get(0).getLabel()).isEqualTo("CtLocalVariable (role: statement) : a");
		assertThat(res.getChildren().get(0).getChildren()).hasSize(2);
		assertThat(res.getChildren().get(0).getChildren().get(0).getLabel()).isEqualTo("CtTypeReference (role: type) : int");
		assertThat(res.getChildren().get(0).getChildren().get(0).getChildren()).hasSize(0);
		assertThat(res.getChildren().get(0).getChildren().get(1).getLabel()).isEqualTo("CtLiteral (role: defaultExpression) : 1");
		assertThat(res.getChildren().get(0).getChildren().get(1).getChildren()).hasSize(1);
		assertThat(res.getChildren().get(0).getChildren().get(1).getChildren().get(0).getLabel()).isEqualTo("CtTypeReference (role: type) : int");
	}

	@Test
	void buildEmptyStatement() {
		analyser = new SpoonTreeCmdBase(true, "", TreeLevel.STATEMENT);
		final var res = analyser.execute().orElseThrow();
		assertThat(res.getChildren()).hasSize(0);
	}

	@Test
	void buildExpressionLevel() {
		analyser = new SpoonTreeCmdBase(true, "1;", TreeLevel.EXPRESSION);
		final var res = analyser.execute().orElseThrow();
		assertThat(res.getChildren()).hasSize(1);
		assertThat(res.getChildren().get(0).getLabel()).isEqualTo("CtLiteral (role: expression) : 1");
		assertThat(res.getChildren().get(0).getChildren()).hasSize(1);
		assertThat(res.getChildren().get(0).getChildren().get(0).getLabel()).isEqualTo("CtTypeReference (role: type) : int");
		assertThat(res.getChildren().get(0).getChildren().get(0).getChildren()).hasSize(0);
	}

	@Test
	void setCodeTest() {
		analyser = new SpoonTreeCmdBase(true, "public class Foo {int foo1; String foo2}", TreeLevel.CLASS_ELEMENT);
		analyser.setCode("public class Foo {}");
		final var res = analyser.execute().orElseThrow().getChildren().get(0).getChildren();
		assertThat(res).hasSize(0);
	}

	@Test
	void buildAutoElement() {
		analyser = new SpoonTreeCmdBase(true, "int foo1;", TreeLevel.AUTO);
		final var res = analyser.execute().orElseThrow();
		assertThat(res.getChildren()).hasSize(1);
		assertThat(res.getChildren().get(0).getLabel()).isEqualTo("CtField (role: typeMember) : foo1");
		assertThat(res.getChildren().get(0).getChildren()).hasSize(1);
		assertThat(res.getChildren().get(0).getChildren().get(0).getLabel()).isEqualTo("CtTypeReference (role: type) : int");
	}
}
