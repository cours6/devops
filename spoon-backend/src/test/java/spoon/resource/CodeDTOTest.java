package spoon.resource;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CodeDTOTest {

    CodeDTO codeDTO;

    @BeforeEach
    void setUp() {
        this.codeDTO = new CodeDTO("code", "level");
    }

    @Test
    void getCode() {
        assertEquals("code", this.codeDTO.getCode());
    }

    @Test
    void setCode() {
        assertEquals("code", this.codeDTO.getCode());
        this.codeDTO.setCode("newcode");
        assertEquals("newcode", this.codeDTO.getCode());
    }

    @Test
    void getLevel() {
        assertEquals("level", this.codeDTO.getLevel());
    }

    @Test
    void setLevel() {
        assertEquals("level", this.codeDTO.getLevel());
        this.codeDTO.setLevel("newlevel");
        assertEquals("newlevel", this.codeDTO.getLevel());
    }
}