# Report

## Backend
- Tests unitaires
- Checkstyle (./spoon_backend/checkstyle.xml)
- Linters : spotbugs + errorprone (pom.xml)
- Code coverage : 84% (line)
- Muntation avec Descartes

## Frontend
- Tests unitaires
    - AppComponent  : src\app\app.component.spec.ts
    - AstComponent  : src\app\component\ast\ast.component.spec.ts
    - UpdateCode    : src\app\command\update-code.spec.ts
- Test e2e
    - AppPage       : e2e\src\app.po.ts
    - App           : e2e\src\app.e2e-spec.ts
- Linters avec eslint (default)
- Code coverage (voir karma.conf.js) :  
    - Statements    : 96% ( 24/25 )
    - Branches      : 100% ( 2/2 )
    - Functions     : 90% ( 9/10 )
    - Lines         : 95% ( 19/20 )

## Build
- Voir: .gitlab-ci.yml 

## Package
- Backend: ./spoon-backend/Dockerfile
- Frontend: ./spoon-frontend/Dockerfile

## Deploy
- Each build success --> Images created
- Run the project : Minikube - voir ./helm et operate ou docker-compose pour tester
- Load Balancing: services kubernetes ./helm/template
- Reverse proxy: ingress ./helm/template
- Rolling update: fonctionnement par défaut kubernetes
- ./helm/values.yml concentration des variables des fichiers kubernetes

## Operate 
- A/B Testing : Minikube - voir ./helm
```
cd helm
minikube start
minikube addons enable ingress
//Il faut avoir l'autorisation de pull les images
helm install app .
```
Note: Need the project to be in public in order to request the images.